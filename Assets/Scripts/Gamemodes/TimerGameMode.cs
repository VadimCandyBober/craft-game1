﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimerGameMode : Mode {

    public static TimerGameMode _instance;

    public int waveCount = 8;
    public List<Wave> _waves = new List<Wave>();
    int _enemysSpawned = 0;
    int nowWave = 0;
    int timeNow = 0;
    public GameObject _modeUI;
    public Text _timerText;

    public GameObject _modeEndUI;

    public Text _endText;

    float timerPercent;
	// Use this for initialization
	void Start () 
    {
        _instance = this;
	}

    public override void StartMode()
    {
        StartGame();
        EnemyParamsSetUp();
        _modeUI.SetActive(true);
        StartCoroutine(EnemySpawnedUpdate());
        _timerText.text = timeNow.ToString();
        base.StartMode();
    }

    private void EnemyParamsSetUp()
    {
        EnemyManager._instance._enemyParams.hp = _waves[nowWave]._enemysHp;
        EnemyManager._instance._enemyParams.damage = _waves[nowWave]._enemysDamage;
    }

	public void StartGame()
    {
        StartCoroutine(GameLoop());
    }

    IEnumerator GameLoop()
    {
        yield return new WaitForSeconds(1);
        timeNow++;
    
        _timerText.text = timeNow.ToString();
         StartCoroutine(GameLoop());
    }

    public void GameEnd()
    {
        Time.timeScale = 0;
        _modeUI.SetActive(false);
        GameHandler._instance.PlayerUI.SetActive(false);
        PlayerStats._instance._camera.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = true;
        _modeEndUI.SetActive(true);
        _endText.text = "You lasted " + timeNow + " seconds ";
    }

    IEnumerator EnemySpawnedUpdate()
    {
        _enemysSpawned = EnemyManager._instance.GetSpawnedEnemy();
        yield return new WaitForSeconds(0.5f);
        if(_enemysSpawned > _waves[nowWave].enemysInWave)
        {
            nowWave++;
            if (nowWave >= _waves.Count) nowWave = _waves.Count - 1;
            EnemyManager._instance.StopAllCoroutines();
            if(EnemyManager._instance.GetAllEnemys().Count < _waves[nowWave].enemysInWave)
            {
                EnemyManager._instance.EnemyListExplain(_waves[nowWave].enemysInWave -EnemyManager._instance.GetAllEnemys().Count );
            }
            EnemyParamsSetUp();
            EnemyManager._instance.enemySpawnDelay = _waves[nowWave]._enemysSpawnDelay;
            EnemyManager._instance.StartSpawnEnemy();
        }
            
        StartCoroutine(EnemySpawnedUpdate());
    }

	void Update () 
    {
      

	}

}
