﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieCountMode : Mode {


    public static ZombieCountMode _instance;
    public float GameTime;

    public int waveCount = 8;
    public Wave wave;
    int _enemysSpawned = 0;
    int _killedZombie;

    public GameObject _modeUI;
    public GameObject _modeEndUI;

    public Text _endText;
    public Text _timerText;
    public Image _timerImage;


    float timerPercent;
    // Use this for initialization
    void Start()
    {
        _instance = this;

    }

    public override void StartMode()
    {
        StartGame();
        EnemyParamsSetUp();
        _modeUI.SetActive(true);
        timerPercent = 1 / GameTime;
        _timerText.text = GameTime.ToString();
        base.StartMode();
    }

    private void EnemyParamsSetUp()
    {
        EnemyManager._instance._enemyParams.hp = wave._enemysHp;
        EnemyManager._instance._enemyParams.damage = wave._enemysDamage;
        EnemyManager._instance.enemySpawnDelay = 1.2f;
    }

    public void StartGame()
    {
        StartCoroutine(GameLoop());
    }

    IEnumerator GameLoop()
    {
        yield return new WaitForSeconds(1);
        GameTime--;
        _timerImage.fillAmount -= timerPercent;
        _timerText.text = GameTime.ToString();
        if (GameTime <= 0)
            GameEnd();
        else StartCoroutine(GameLoop());
    }

    public void GameEnd()
    {
        _modeUI.SetActive(false);
        Time.timeScale = 0;
        GameHandler._instance.PlayerUI.SetActive(false);
        PlayerStats._instance._camera.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = true;
        _modeEndUI.SetActive(true);
        _endText.text = "You kill " + PlayerStats._instance.PointsGet() + " zombies ";
    }




    void Update()
    {


    }
}
