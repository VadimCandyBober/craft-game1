﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Mode : MonoBehaviour {

    public virtual void StartMode()
    {
        Debug.Log("Game mode started");
    }

}
