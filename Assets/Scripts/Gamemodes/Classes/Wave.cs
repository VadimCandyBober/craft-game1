﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wave 
{
    public int enemysInWave;
    public int _enemysHp;
    public int _enemysDamage;
    public int _enemysDefence;
    public float _enemysSpeed;
    public float _enemysSpawnDelay;
}
