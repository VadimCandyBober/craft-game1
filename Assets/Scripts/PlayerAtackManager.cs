﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAtackManager : MonoBehaviour {

    public float _attackDelay = 0.5f ;
    bool _canAttack = true;
    bool _Attack = false;
    public static PlayerAtackManager _instance;
	// Use this for initialization
	void Start () {
        _instance = this;
	}
	
	// Update is called once per frame
	void Update () {
        if(ScreenClick._instance.GetClickUp() && ScreenClick._instance.GetClickNum()==0 &&_canAttack)
        {
            _Attack = true;
            PlayerAnimator._instance.Attack();
            Attack();
  
        }
	}

    public bool GetAttack()
    {
        return _Attack;
    }

    public void Attack()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, fwd, out hit, 6))
        {
            if (hit.collider.tag == "Enemy") 
            {
                hit.collider.GetComponent<EnemyStats>().TakeDamage(10, transform);
                _canAttack = false;
                StartCoroutine(AtackDelay());
            }
        }
        _Attack = false;
    }

    IEnumerator AtackDelay()
    {
        yield return new WaitForSeconds(_attackDelay);
        _canAttack = true;
    }
}
