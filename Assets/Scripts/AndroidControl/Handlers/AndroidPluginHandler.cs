﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AndroidPluginHandler : MonoBehaviour {

    public Text text;
	// Use this for initialization
	void Start () {
        var plugin = new AndroidJavaClass("com.example.unityplugin.PluginClass");
       text.text = plugin.CallStatic<string>("GetTextFromPlugin", 2);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
