﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Threading;
public class SwipeHandler : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Vector3 startPos = Vector3.zero;
    private Vector3 endPos = Vector3.zero;
    private Vector3 endPosOld = Vector3.zero;
    bool _posDetect,_notMoving;
    PointerEventData _ped;
    public static SwipeHandler _instance;
    public virtual void OnDrag(PointerEventData eventData)
    {
        endPos = eventData.position;
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        startPos = ped.position;
        endPosOld = startPos;
        _ped = ped;
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {


        _posDetect = false;
        startPos = Vector3.zero;
        endPos = Vector3.zero;
    }

    // Use this for initialization
    void Start () {
        _instance = this;
	}
	
	// Update is called once per frame
	void Update () {
        if (_ped != null)
            _notMoving = _ped.delta.x > 2 || _ped.delta.x < -2 ? true : false;
        if (!_notMoving) startPos = endPos;
    }

    public float GetHorizontal()
    {
      //  if (ScreenClick._instance.GetdDrug() && _notMoving)
       // {
            Vector3 r = endPos - startPos;
            r = r.normalized;
            return r.x;
       // }
       // else return 0;
    }
}
