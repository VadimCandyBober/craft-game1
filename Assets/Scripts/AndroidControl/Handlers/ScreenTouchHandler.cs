﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Threading;
public class ScreenTouchHandler : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{

    private Vector3 startPos = Vector3.zero;
    private Vector3 endPos = Vector3.zero;
   // public Image touchImage;
    Vector3 _inputVector;
    private Image _backGroundImage;
    private Vector3 endPosOld = Vector3.zero;
    bool _posDetect, _notMoving;
    [HideInInspector]
    public PointerEventData _ped;
    public static ScreenTouchHandler _instance;
    private void Start()
    {
        _backGroundImage = GetComponent<Image>();
        _instance = this;
    }
    private void Update()
    {
        if (_ped != null)
            _notMoving = _ped.delta.y > 2 || _ped.delta.y < -2 ? true : false;
        if (!_notMoving) startPos = endPos;
    }
    public virtual void OnDrag(PointerEventData eventData)
    {

        _inputVector = TouchManager.DragProcessing(_backGroundImage, eventData, 0,0);
      //  touchImage.rectTransform.anchoredPosition = new Vector3(_inputVector.x * (
       //     _backGroundImage.rectTransform.sizeDelta.x/2), _inputVector.z * (_backGroundImage.rectTransform.sizeDelta.y/2));
        endPos = eventData.position;
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
    
       _ped = ped;
        //touchImage.enabled = true;
        startPos = ped.position;
        endPosOld = startPos;
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
     
        _posDetect = false;
      //  touchImage.enabled = false;
        startPos = Vector3.zero;
        endPos = Vector3.zero;
        _notMoving = false;
    }

    public float GetVertical()
    {
     //   if (ScreenClick._instance.GetdDrug() && _notMoving)
       // {
            Vector3 r = endPos - startPos;
            r = r.normalized;
            return r.y;
       // }
       // else return 0;
    }
}
