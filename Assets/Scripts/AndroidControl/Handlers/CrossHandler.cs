﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

enum State : int
{
    up = 0,
    down = 1,
    left = 2,
    right = 3,
    upLeft = 4,
    upRight = 5,
    downLeft = 6,
    downRight = 7,
    none
}
public class CrossHandler : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Image _backGroundImage;
    private Vector3 _inputVector;
    public static CrossHandler _instance;
    public GameObject _Up, _Down;
    public Image[] images;
    private State state;
    float timeToClick;
    bool sprint, pointerDowned;

    private void Start()
    {
        _instance = this;
        _backGroundImage = GetComponent<Image>();
    }

    public void ImageProcessing()
    {
        if (_inputVector.x < 0.5f && _inputVector.x > -0.5f)
        {
            if (_inputVector.z > 0.4f) state = State.up;
            else if (_inputVector.z < -0.5f) state = State.down;
            else state = State.none;
        } else
        {
            if (_inputVector.z > 0.5f)
            {
                if (_inputVector.x > 0.5f) state = State.upRight;
                else if (_inputVector.x < -0.5f) state = State.upLeft;
                else state = State.none;
            }
            else if (_inputVector.z < -0.5f)
            {
                if (_inputVector.x > 0.5f) state = State.downRight;
                else if (_inputVector.x < -0.5f) state = State.downLeft;
                else state = State.none;
            }
            else if (_inputVector.x > 0.5f) state = State.right;
            else if (_inputVector.x < -0.5f) state = State.left;
            else state = State.none;
        }

        foreach (Image i in images)
            i.sprite = SpriteChanger._instance.SpriteChange(false);
        if (state != State.none) images[(int)state].sprite = SpriteChanger._instance.SpriteChange(true);

    }

    public virtual void OnDrag(PointerEventData ped)
    {
        _inputVector = TouchManager.DragProcessing(_backGroundImage, ped, -1, 1);

        if (_inputVector.z > 0.5f)
        {
            _Up.SetActive(true);
            _Down.SetActive(false);
        }

        if (_inputVector.z < -0.5f)
        {
            _Up.SetActive(false);
            _Down.SetActive(true);
        }

        if (_inputVector == Vector3.zero)
        {
            _Up.SetActive(false);
            _Down.SetActive(false);
        }
    }
    private void Update()
    {
       
        if (_inputVector == Vector3.zero)
        {
            _Up.SetActive(false);
            _Down.SetActive(false);
        }

        if (pointerDowned)
        {
            ImageProcessing();
            GameLogicProcessing();
        }
    }

    private void GameLogicProcessing()
    {
        timeToClick += Time.deltaTime;

    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
     
        _inputVector = TouchManager.DragProcessing(_backGroundImage, ped, -1, 1);
        sprint = timeToClick < 0.2f && timeToClick != 0 && _inputVector.z > 0.4f ? true : false;
        timeToClick = 0;
        pointerDowned = true;
        OnDrag(ped);
    }


    public virtual void OnPointerUp(PointerEventData ped)
    {
        _inputVector = Vector3.zero;
        sprint = false;

        timeToClick = 0;
    }

    public bool GetSprint()
    {
        return sprint;
    }

    public float GetHotizontal()
    {
        return _inputVector.x != 0 ? _inputVector.x : Input.GetAxis("Horizontal");
    }
    public float GetVertical()
    {
        return _inputVector.z != 0 ? _inputVector.z : Input.GetAxis("Vertical");
    }
}
