﻿
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;
using System;

public class JoystickHandler : MonoBehaviour, IDragHandler, IPointerUpHandler,IPointerDownHandler{

    private Image _backGroundImage;
    private Image _joystickImage;
    private Vector3 _inputVector;
    public static JoystickHandler _instance;
    private void Start()
    {
        _instance = this;
        _backGroundImage = GetComponent<Image>();
        _joystickImage = transform.GetChild(0).GetComponent<Image>();
    }
  
    public virtual void OnDrag(PointerEventData ped)
    {
            _inputVector = TouchManager.DragProcessing(_backGroundImage,ped,-1,1);
            _joystickImage.rectTransform.anchoredPosition = new Vector3(_inputVector.x * (
                _backGroundImage.rectTransform.sizeDelta.x / 3), _inputVector.z * (_backGroundImage.rectTransform.sizeDelta.y / 3));
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    public virtual void OnPinnter(PointerEventData ped)
    {
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        _inputVector = Vector3.zero;
        _joystickImage.rectTransform.anchoredPosition = Vector3.zero;
    }

    public float GetHotizontal()
    {
        return _inputVector.x != 0 ? _inputVector.x : Input.GetAxis("Horizontal");
    }
    public float GetVertical()
    {
        return _inputVector.z != 0 ? _inputVector.z : Input.GetAxis("Vertical");
    }
}
