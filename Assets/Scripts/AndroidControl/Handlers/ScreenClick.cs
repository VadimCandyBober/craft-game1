﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Threading;
public class ScreenClick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Image _backGroundImage;
    private Image _toTouchImage;
    bool _clickUp = false;
    bool _holding, _longTouch, _following, _canBuild, _drug;
    float _pressTime, _buildTime;
    int clickNum = 0; // 0 - fast Click 1 - lowClick
    private Vector3 _touchPos, _startPos, _endPos;
    public float _NeedPressTime = 0.3f;
    public float _NeedBuildTime = 0.5f;
    public static ScreenClick _instance;
    public Image touchInsadeImage;
    // Use this for initialization
    void Start() {
        _instance = this;
        _backGroundImage = GetComponent<Image>();
    }

    public virtual void OnDrag(PointerEventData ped)
    {
        _endPos = ped.position;
        //  Debug.Log(ped.delta.magnitude);
        if (ped.delta.magnitude > 1) _drug = true;

    }

    public void SetTouchImage(Image _img)
    {
        _toTouchImage = _img;
    }

    private void FastClick()
    {
        clickNum = 0;
    }

    private void LowClick()
    {
        clickNum = 1;
    }

    public bool GetdDrug()
    {
        return _drug;
    }

    private void Update()
    {
        TouchHold();
    }

    private void TouchHold()
    {
        if (_holding)
        {
            _pressTime += Time.deltaTime;
            if (!GetdDrug()) _buildTime += Time.deltaTime;
            else _buildTime = 0;
            if (_buildTime > _NeedBuildTime)
            {
                _canBuild = true;
            }
            if (_pressTime > _NeedPressTime) {
                _longTouch = true;
                _pressTime = 0;
                LowClick();
            }
        } else _buildTime = 0;
       // touchInsadeImage.fillAmount = _buildTime * 1.5f;
    }
    public virtual void OnPointerDown(PointerEventData ped)
    {
        _startPos = ped.position;
        _endPos = ped.position;
        touchInsadeImage.enabled = true;
        _following = true;
        _drug = false;
        if (TouchManager.InActiveArea(_backGroundImage, ped))
        {
            _clickUp = true;
            _holding = true;
        }
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {


        _startPos = Vector3.zero;
        _endPos = Vector3.zero;
        touchInsadeImage.enabled = false;
        Thread clickOffThread = new Thread(new ThreadStart(ClickUpFalse));
        clickOffThread.Start();
        _pressTime = 0;
        _holding = false;
        _following = false;

    }

    private void ClickUpFalse()
    {
        if (!_longTouch)
        {
            FastClick();
            Thread.Sleep(100);
            _clickUp = false;
        }
        _longTouch = false;
    }

    #region GetsFunctions 

    public bool GetTouchPos(out Vector3 pos)
    {
        pos = _touchPos;
        return _holding;
    }

    public float GetBuildTime()
    {
        return _buildTime;
    }

    public bool GetBuild()
    {
        bool temp = _canBuild;
        if (_canBuild)
        {
            _canBuild = false;
            _buildTime = 0;
        }
        return temp;
    }

    public bool GetClick()
    {
        return _holding;
    }
    public bool GetClickUp()
    {
        return _clickUp;
    }
    private bool GetLongTouch()
    {
        return _longTouch;
    }
    public int GetClickNum()
    {
        return clickNum;
    }
    #endregion
}
