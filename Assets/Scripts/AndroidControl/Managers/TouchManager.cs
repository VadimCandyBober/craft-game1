﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class TouchManager : MonoBehaviour {

    public static Vector3 DragProcessing(Image _backGroundImage, PointerEventData ped, float offset1, float offset2)
    {
        Vector2 pos;
        Vector3 _inputVector = Vector3.zero ;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_backGroundImage.rectTransform, ped.position, ped.pressEventCamera, out pos))
        {
            pos.x = (pos.x / _backGroundImage.rectTransform.sizeDelta.x);
            pos.y = (pos.y / _backGroundImage.rectTransform.sizeDelta.y);

            _inputVector = new Vector3(pos.x * 2 + offset1, 0, pos.y * 2 - offset2);
            _inputVector = (_inputVector.magnitude > 1.0f) ? _inputVector.normalized : _inputVector;
        }
        return _inputVector;
    }

    public static bool InActiveArea(Image _backGroundImage, PointerEventData ped)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_backGroundImage.rectTransform, ped.position, ped.pressEventCamera, out pos))
            return true;
        else return false;
    }

}
