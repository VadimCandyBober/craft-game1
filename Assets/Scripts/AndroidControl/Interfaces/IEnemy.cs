﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemy {
    int damage { get; set; }
    float atackDelay { get; set; }
    float atackDistance { get; set; }
    float DistanceToPlayer(Transform player);
    void Atack();



}
