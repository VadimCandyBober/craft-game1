﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHumanoid {
    float moveSpeed { get; set; }
    float jumpPower { get; set; }
    void Jump();
    void Move(Vector3 _moveDir);
}
