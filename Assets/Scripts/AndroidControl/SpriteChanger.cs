﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpriteChanger : MonoBehaviour {

    public Sprite[] sprites;
    public static SpriteChanger _instance;
	// Use this for initialization
	void Start () {
        _instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public Sprite SpriteChange(bool active)
    {
        return active ? sprites[0] : sprites[1];
     }

    
}
