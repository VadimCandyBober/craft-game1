﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform player;
    public float _YSensetive = 1f, _XSensetive = 1f;
    public float _Ymax, _Ymin;
    private float rotationY;
   
    // Use this for initialization
    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update () {
      
        player.Rotate(new Vector3(0, SwipeHandler._instance.GetHorizontal() * _XSensetive, 0));
        rotationY += ScreenTouchHandler._instance.GetVertical() * _YSensetive;
        rotationY = Mathf.Clamp(rotationY, _Ymin, _Ymax);

        transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, transform.localEulerAngles.z);
    }
}
