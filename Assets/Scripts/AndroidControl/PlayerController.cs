﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    // These variables are for adjusting in the inspector how the object behaves 
    public float maxSpeed = 7;
    public float force = 8;
    public float jumpSpeed = 5;

    // These variables are there for use by the script and don't need to be edited
    private int state = 0;
    private bool grounded = false;
    private float jumpLimit = 0;
    private Rigidbody rigidbody;

    public static PlayerController _instance;
    public bool _Jump;

    // Don't let the Physics Engine rotate this physics object so it doesn't fall over when running
    void Awake()
    {
        _instance = this;
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.freezeRotation = true;
    }

    // This part detects whether or not the object is grounded and stores it in a variable
    void OnCollisionEnter()
    {
        state++;
        if (state > 0)
        {
            grounded = true;
        }
    }


    void OnCollisionExit()
    {
        state--;
        if (state < 1)
        {
            grounded = false;
            state = 0;
        }
    }


    public virtual bool jump
    {
        get
        {
            return _Jump;
        }
        set
        {
            _Jump = value;
        }
    }

    public virtual float horizontal
    {
        get
        {
            return JoystickHandler._instance.GetHotizontal() * force;
        }
    }
    public virtual float vertical
    {
        get
        {
            return JoystickHandler._instance.GetVertical() * force;
        }
    }
    public void Jump()
    {
        jump = true;
    }
    void FixedUpdate()
    {


        if (horizontal != 0 || vertical != 0)
            PlayerAnimator._instance.Move();
        else PlayerAnimator._instance.Idle();
    // If the object is grounded and isn't moving at the max speed or higher apply force to move it
    if(rigidbody.velocity.magnitude < maxSpeed && grounded == true)
    {
        rigidbody.AddForce (transform.rotation * Vector3.forward * vertical);
        rigidbody.AddForce (transform.rotation * Vector3.right * horizontal);
    }
 
    // This part is for jumping. I only let jump force be applied every 10 physics frames so
    // the player can't somehow get a huge velocity due to multiple jumps in a very short time
    if(jumpLimit < 10) jumpLimit ++;
 
    if(jump && grounded  && jumpLimit >= 10)
    {
        rigidbody.velocity = rigidbody.velocity + (Vector3.up * jumpSpeed);
        jumpLimit = 0;
    }
 }
 
}
