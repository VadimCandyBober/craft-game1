﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;
public class TouchImage : MonoBehaviour {
    private Image toTouchImage;
    // Use this for initialization
    void Start()
    {
        toTouchImage = GetComponent<Image>();
        Thread thread = new Thread(ScreenClickStart);
        thread.Start();
    }
		
    public void ScreenClickStart()
    {
        Thread.Sleep(100);
        ScreenClick._instance.SetTouchImage(toTouchImage);
    
}
	// Update is called once per frame
	void Update () {
		
	}
}
