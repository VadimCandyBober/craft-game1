﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FollowPoint : MonoBehaviour {

    public Canvas myCanvas;
    
	// Use this for initialization
	void Start () {
      
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonUp(0)) transform.GetComponent<Image>().enabled = false;
        else if(Input.GetMouseButtonDown(0))transform.GetComponent<Image>().enabled = true;
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(myCanvas.transform as RectTransform, Input.mousePosition, myCanvas.worldCamera, out pos);
        transform.position = myCanvas.transform.TransformPoint(pos);
    }
}
