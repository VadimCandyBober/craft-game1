﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanoidController : MonoBehaviour, IHumanoid {

    public float moveSpeed { get; set; }

    public float jumpPower { get; set; }

    public HumanoidController(float _moveSpeed, float _jumpPower)
    {
        moveSpeed = _moveSpeed;
        jumpPower = _jumpPower;
    }


	void Update () {
		
	}


    public virtual void Move(Vector3 _moveDir)
    {
        GetComponent<Rigidbody>().AddForce(transform.TransformDirection(_moveDir)* moveSpeed);
    }

    public virtual void Jump()
    {
        Vector3 dwn = transform.TransformDirection(Vector3.down);
        if (Physics.Raycast(transform.position, dwn, 1))
        {
            Debug.Log("JUMP");
            GetComponent<Rigidbody>().AddForce(Vector3.up * jumpPower);
        }
    }
}
