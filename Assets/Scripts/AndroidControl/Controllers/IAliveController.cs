﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAliveController : MonoBehaviour, IAlive {

    private float dieDelay = 1f; 

    public virtual IEnumerator Die()
    {
        yield return new WaitForSeconds(dieDelay);
        Destroy(gameObject);
    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
