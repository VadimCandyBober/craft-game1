﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuManager : MonoBehaviour {

    public GameObject[] _screens;
	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void ChangeScreen(int screen)
    {
        for (int i = 0; i < _screens.Length; i++)
            _screens[i].SetActive(false);

        _screens[screen].SetActive(true);  

    }

    public void ModeChange(int mode)
    {
        PlayerPrefs.SetInt("Mode",mode);
        PlayerPrefs.Save();
        StartGame();
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
