﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("StartGame",0.2f);
	}

    public void StartGame()
    {
        GameHandler._instance.OnWorldGenerated();
        ModeManager._instance.ModeSetUp();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
