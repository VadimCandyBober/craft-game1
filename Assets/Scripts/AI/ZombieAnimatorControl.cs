﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieAnimatorControl : MonoBehaviour {

    private Animator _anim;
	// Use this for initialization
	void Start () 
    {
        _anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}
    public void GetDamage()
    {
       _anim.SetInteger("State", 4);
    }
    public void Attack()
    {
        _anim.SetInteger("State", 3);
    }
    public void Move()
    {
        _anim.SetInteger("State",1);
    }
    public void Die()
    {
        _anim.SetInteger("State", 2);
    }

    public void BowShoot()
    {
        _anim.SetInteger("State", 5);
    }


    public void Idle()
    {
        _anim.SetInteger("State",0);
    }

}
