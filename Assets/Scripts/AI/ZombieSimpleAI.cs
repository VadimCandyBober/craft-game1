﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class ZombieSimpleAI : MonoBehaviour
{
    private Pathfinding agent;
    private ZombieAnimatorControl _animator;
    public GameObject _player;

    public int _AtackDelay = 1;
    public float _AtackDistance = 2;
    bool _canAttack = true;
    public int Damage;

    private bool ISDefaultSystem = true;
    // Use this for initialization
    void Start()
    {
        agent = GetComponent<Pathfinding>();
        _animator = GetComponentInChildren<ZombieAnimatorControl>();
    }

    public void SetDefaaultSystem(bool valuer)
    {
        ISDefaultSystem = !valuer;
    }

    // Update is called once per frame
    void Update()
    {
        if (ISDefaultSystem)
        {
            if (_player)
            {
                if (GetDistance() < _AtackDistance)
                {
                    if (_canAttack)
                    {
                        PlayerStats._instance.TakeDamage(Damage);
                        _canAttack = false;
                        StartCoroutine(atackDelay());
                        _animator.Attack();
                    }
                }
                else
                {
                    Navigation();
                }

            }
            else _player = GameObject.FindGameObjectWithTag("Player");

        }
    }

    IEnumerator atackDelay()
    {
        yield return new WaitForSeconds(_AtackDelay);
        _canAttack = true;
    }


    private float GetDistance()
    {
        return Vector3.Distance(transform.position, _player.transform.position);
    }

    private void Navigation()
    {
        _animator.Move();
        agent.NavigateToPlayer();
    }
}
