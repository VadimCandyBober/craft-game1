﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
public class BuildMeshBlock : MonoBehaviour
{
    public GameObject newBlock;
    public bool random = false;
    public bool randomObstacle = false;
    public bool NeedWorld;    
    public int obstaclePrecent = 60;
    public List<LayersUps> _layers = new List<LayersUps>();
    public Vector2 uvMain;
    public GameObject[] Obstacles;
    void Start()
    {

        GetComponent<SetUVs>().tileX = 2;
        GetComponent<SetUVs>().tileY = 15;
        GetComponent<SetUVs>().StetUP();

        if (random)
            _layers = GetComponent<RandomAlghoritmManager>()._layers;
        StartCoroutine(WorldGeneration());

    }

    IEnumerator WorldGeneration()
    {

        for (int k = 0; k < _layers.Count; k++)
        {
            for (int i =  0 ; i <_layers[k]._layersUp.Count ; i++)
            {
                for (int j = 0; j < _layers[k]._layersUp[i].layers.Count; j++)
                {
                    if (!randomObstacle)
                    {
                        GenerationWithoutObstacle(k,i,j);
                        
                    } else 
                    {
                        GenerationWithObstacle(obstaclePrecent,k,i,j);
                    }
               
                }
                yield return null;
            }
       
        }
            MeshCollider[] colliders = GetComponents<MeshCollider>();
            for (int i = 0; i < colliders.Length - 1; i++)
            {
                Destroy(colliders[i]);
            }
            CombineMesh();

        if (NeedWorld)
        {
            NavigationBaker._instance.Bake();
            GameHandler._instance.OnWorldGenerated();
            ModeManager._instance.ModeSetUp();
        }
        Debug.Log("GENERATED");
        yield return null;
    }

    public void GenerationWithObstacle(int probability, params int[] position)
    {
        int IsHole = UnityEngine.Random.Range(0, 100 - probability);
        IsHole = IsHole <= 0 ? 1 : IsHole;
        Vector3 blockPos = new Vector3(position[0], position[1], position[2]);
        if (IsHole == 1)
            BuildBlock(blockPos, _layers[position[0]]._layersUp[position[1]].layers[position[1]].uvs);
    }

    public void GenerationWithoutObstacle(params int[] position)
    {
        Vector3 blockPos = new Vector3(position[0], position[1], position[2]);
        if (!_layers[position[0]]._layersUp[position[1]].layers[position[2]]._isHole)
            BuildBlock(blockPos, _layers[position[0]]._layersUp[position[1]].layers[position[2]].uvs);
    }



    void CombineMesh()
    {
        MeshFilter[] meshFiltres = GetComponentsInChildren<MeshFilter>();
        CombineInstance[] combine = new CombineInstance[meshFiltres.Length];
        int i = 0;
        while (i < meshFiltres.Length)
        {
            combine[i].mesh = meshFiltres[i].sharedMesh;
            combine[i].transform = meshFiltres[i].transform.localToWorldMatrix;
            meshFiltres[i].gameObject.SetActive(false);
     
            i++;
        }
        foreach (Transform child in gameObject.transform)
        {
            Destroy(child.gameObject);
        }
        MeshFilter _mFilter = transform.GetComponent<MeshFilter>();
        _mFilter.mesh = new Mesh();
        _mFilter.mesh.CombineMeshes(combine,true);
        _mFilter.mesh.RecalculateBounds();
        _mFilter.mesh.RecalculateNormals();
        this.gameObject.AddComponent<MeshCollider>();
     
        transform.gameObject.SetActive(true);
    }

    public void BuildBlock(Vector3 blockPos, Vector2 uv)
    {
        blockPos.x = (float)Math.Round(blockPos.x, MidpointRounding.AwayFromZero);
        blockPos.y = (float)Math.Round(blockPos.y, MidpointRounding.AwayFromZero);
        blockPos.z = (float)Math.Round(blockPos.z, MidpointRounding.AwayFromZero);
        GameObject block;
        if (!randomObstacle)
            block = Instantiate(newBlock, blockPos, Quaternion.identity);
        else block = Instantiate(Obstacles[UnityEngine.Random.Range(0,Obstacles.Length)], blockPos, Quaternion.identity);

        block.transform.position = blockPos;
        block.transform.rotation = Quaternion.identity;
        if (random)
        {
            block.GetComponent<SetUVs>().tileX = uv.x;
            block.GetComponent<SetUVs>().tileY = uv.y;
        } else if(!randomObstacle)
        {
            block.GetComponent<SetUVs>().tileX = uvMain.x;
            block.GetComponent<SetUVs>().tileY = uvMain.y ;
        }
        if(!randomObstacle)
        block.GetComponent<SetUVs>().StetUP();
        block.transform.parent = this.transform;

    }

   
}
