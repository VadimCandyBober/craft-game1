﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ActCollection {
    public List<Act> _acts = new List<Act>();

}
