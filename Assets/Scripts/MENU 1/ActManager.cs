﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActManager : MonoBehaviour {

    public ActCollection _actCollection;
    private List<Act> _acts = new List<Act>();

    public Sprite[] _lockOrUnlock;



    private void AllActsSet()
    {
        PlayerPrefs.SetString("JsonAct", JsoHelper.ConvertToJson(_actCollection));
        PlayerPrefs.Save();
    }

    private void AllActsGet()
    {
        _actCollection = JsoHelper.ToObject<ActCollection>(PlayerPrefs.GetString("JsonAct"));
        _acts = _actCollection._acts;
    }

    private void SetUpsLevels()
    {
        for (int i = 0; i < _acts.Count; i ++)
            _acts[i]._LockImage.sprite = _acts[i]._UnLocked ? _lockOrUnlock[1] : _lockOrUnlock[0];
    }

    public void ActClick(int actID)
    {
        Act a = new Act();
        for (int i = 0; i < _acts.Count; i++) if (_acts[i]._ID == actID) a = _acts[i];
    }

    private void Start()
    {
        
    }





}
