﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class Act
{
    public int _ID;
    public int _SceneId;
    public bool _UnLocked;
    public Image _LockImage;
    public bool _isEnded;
}
