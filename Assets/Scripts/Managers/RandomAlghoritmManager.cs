﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAlghoritmManager : MonoBehaviour {

    public List<LayersUps> _layers = new List<LayersUps>();

    public int unevennessesLevel = 14;

    public int worldSize = 8;

    public Vector2[] uvs;
    private void Awake()
    {
        Generate();
    }
    void Start () 
    {
       

	}

    public int RandomPointToRock()
    {
       int result = Random.Range(0,worldSize);
        while(result - 3 < 0 && result + 3 > 8)
            result = Random.Range(0, worldSize);
        Debug.Log(result);

        return result;
    }


    public void Generate()
    {
        int rndToRock = RandomPointToRock();
        for (int i = 0; i < worldSize; i++)
        {
            LayersUps _layer = new LayersUps();

            if (i < unevennessesLevel)
            {
                for (int j = 0; j < worldSize; j++)
                {
                    LayersForwad _lf = new LayersForwad();
                    for (int k = 0; k < worldSize; k++)
                    {
                        LayerForward l = new LayerForward();
                        l.uvs = uvs[0];
                        _lf.layers.Add(l);
                    }
                    _layer._layersUp.Add(_lf);
                }
            }
            else
            {
                for (int j = 0; j < worldSize; j++)
                {
                    LayersForwad _lf = new LayersForwad();
                    for (int k = 0; k < worldSize; k++)
                    {
                        LayerForward l = new LayerForward();
                        if (i < unevennessesLevel + 2)
                        {
                            if ((k > rndToRock - 4 && k < rndToRock + 4) || (j > rndToRock - 2 && j < rndToRock + 2))
                            {
                                l._isHole = false;
                              
                            }
                            else
                            {
                                l._isHole = true;
                             
                            }
                        }
                        else
                        {
                            if ((k > rndToRock - 2 && k < rndToRock + 2) || (j > rndToRock - 2 && j < rndToRock + 2))
                            {
                                l._isHole = false;
                              
                            }
                            else
                            {
                                l._isHole = true;
                               
                            }
                        }
                        //int hole = Random.Range(0, 2);
                        // l._isHole = System.Convert.ToBoolean(hole);
                        // Debug.Log(l._isHole);
                        l.uvs = uvs[1];
                        _lf.layers.Add(l);
                    }
                    _layer._layersUp.Add(_lf);
                }
            }
           
            _layers.Add(_layer);
            
        }
    }
	void Update () 
    {
		
	}
}
