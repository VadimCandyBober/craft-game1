﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class ItemManager : MonoBehaviour
{

    public GameObject item;
    public int itemSpawnRange = 20;
    public int itemSpawnDelay = 10;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(ItemSpawnLoop());
    }


    IEnumerator ItemSpawnLoop()
    {
        yield return new WaitForSeconds(itemSpawnDelay);
        ItemInst();
        StartCoroutine(ItemSpawnLoop());
    }

    public void ItemInst()
    {
        Instantiate(item, RandomNavmeshLocation(itemSpawnRange), Quaternion.identity);
    }

    public Vector3 RandomNavmeshLocation(float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPosition = hit.position;
        }
        return finalPosition;
    }
}
