﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour {

    public int _health;
    public int MaxHealth = 40;
    bool died = false;
    private Rigidbody rg;
    public int _enemyCost=1;

    public bool Deid {
        get { return died; }
        set { died = value; }
    }
    // Use this for initialization
    void Start()
    {
        rg = GetComponent<Rigidbody>();
        HealthReset(); 
    }
    public void SetUpdStats(int Health)
    {
        MaxHealth = Health;
        _health = MaxHealth;
    }

    void Update()
    {

    }

    public void HealthReset()
    {
        _health = MaxHealth;
    }
    public void TakeDamage(int Damage, Transform player)
    {
        _health -= Damage;
        Vector3 bcw = transform.TransformDirection(Vector3.back);
        GetComponentInChildren<ZombieAnimatorControl>().GetDamage();
        if (_health <= 0 && !died)
        {
            StartCoroutine(Die());
        }
    }

    public IEnumerator Die()
    {
        died = true;
        GetComponentInChildren<ZombieAnimatorControl>().Die();
        ItemHandler._instance.ItemSpawn(transform.position);
        if (GetComponent<ZombieSimpleAI>())
        {
            GetComponent<ZombieSimpleAI>().enabled = false;
            GetComponent<Pathfinding>().enabled = false;
        }
        if(GetComponent<EnemyController>())
        GetComponent<EnemyController>().enabled =false ;

        GetComponent<BoxCollider>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);

        PlayerStats._instance.PointsAdd(_enemyCost);
        HealthReset();
    }
}
