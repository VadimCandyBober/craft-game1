﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    private int points = 0;

    public static PlayerStats _instance;

    public int _health;

    public int _MaxHealth;

    public GameObject _camera;

    // Use this for initialization
    void Start()
    {
        _health = _MaxHealth;

        PlayerUiHandler._instance.PointsUpdate(points);
        _instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public int PointsGet()
    {
        return points;
    }
    public void PointsAdd(int col)
    {
        points += col;
        PlayerUiHandler._instance.PointsUpdate(points);
    }

    public void HpUp(int _value)
    {
        _health += _value;
        if (_health > _MaxHealth) _health = _MaxHealth;
        PlayerUiHandler._instance.HealthBarUpdate(_health / 100.0f);
    }

    public void TakeDamage(int Damage)
    {
        _health -= Damage;
        PlayerUiHandler._instance.HealthBarUpdate(_health/100.0f);
        if (_health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        if (ModeManager._instance.GetChangedMode() != 1)
        {
          //  Time.timeScale = 0;
           // UiManager._instance.PlayerDieUI();
            TimerGameMode._instance.GameEnd();
        } else
        {
            ZombieCountMode._instance.GameEnd();
        }
    }
}
