﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStats{
    int _health { get; set; }
    int _MaxHealth { get; set; }
}