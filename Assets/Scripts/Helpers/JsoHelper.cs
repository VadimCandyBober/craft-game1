﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class JsoHelper {

    public static string ConvertToJson<T>(T convertObject)
    {
         return JsonUtility.ToJson(convertObject);
    }

    public static T ToObject<T>(string json)
    {
        return JsonUtility.FromJson<T>(json);
    }

	
}
