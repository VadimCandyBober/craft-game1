﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public GameObject[] EnemyPrefab;
    public Transform[] _spawn;
    public static EnemyManager _instance;
    public int enemysCount;
    public float enemySpawnDelay;
    private List<GameObject> _enemys = new List<GameObject>();
    int enemySpawned = 0;
    public EnemyParams _enemyParams;
	// Use this for initialization
	void Start () 
    {
        _instance = this;
        GameObjectPoller();
	}

    private void GameObjectPoller()
    {
        for (int i = 0; i < enemysCount; i++)
        {
            int rndEnemy = UnityEngine.Random.Range(0, EnemyPrefab.Length);
            GameObject g = Instantiate(EnemyPrefab[rndEnemy], Vector3.zero, EnemyPrefab[rndEnemy].transform.rotation);
            g.SetActive(false);
            _enemys.Add(g);
        }
    }

    public void EnemyListExplain(int enemyCount)
    {
        for (int i = 0; i < enemysCount; i++)
        {
            int rndEnemy = UnityEngine.Random.Range(0, EnemyPrefab.Length);
            GameObject g = Instantiate(EnemyPrefab[rndEnemy], Vector3.zero, EnemyPrefab[rndEnemy].transform.rotation);
            g.SetActive(false);
            _enemys.Add(g);
        }
    }

    public List<GameObject> GetAllEnemys()
    {
        return _enemys;
    }

    public void StartSpawnEnemy()
    {
        StartCoroutine(SpawnEnemy(enemySpawnDelay));
    }

    public int GetSpawnedEnemy()
    {
        return enemysCount;
    }

    IEnumerator SpawnEnemy(float delay)
    {
        if (enemySpawned < _enemys.Count)
        {
            SpawnZobmie(enemySpawned);
   
        } else 
        {
            for (int i = 0; i < enemysCount; i ++)
            {
                if (_enemys[i].GetComponent<EnemyStats>().Deid)
                {
                    SpawnZobmie(i);
                    break;
                }
            }
        }
        enemySpawned++;
        yield return new WaitForSeconds(delay);
        StartCoroutine(SpawnEnemy(delay));

    }


    public void SpawnZobmie(int num)
    {

        _enemys[num].transform.position = _spawn[UnityEngine.Random.Range(0, _spawn.Length)].position + new Vector3(UnityEngine.Random.Range(-2, 2), 0, UnityEngine.Random.Range(-2, 2));
         EnemyStats e = _enemys[num].GetComponent<EnemyStats>();
         ZombieSimpleAI z = _enemys[num].GetComponent<ZombieSimpleAI>();
         e.Deid = false;
        _enemys[num].GetComponent<BoxCollider>().enabled = true;
        _enemys[num].GetComponent<Rigidbody>().isKinematic = false;
        if(_enemys[num].GetComponent<Pathfinding>()) _enemys[num]. GetComponent<Pathfinding>().enabled = true;
        if(_enemys[num].GetComponent<EnemyController>())
        _enemys[num].GetComponent<EnemyController>().enabled = true;
     
         e.GetComponent<EnemyStats>().MaxHealth += _enemyParams.hp;
      
         e.GetComponent<EnemyStats>().HealthReset();
        if (z)
        {
            z.enabled = true;
            z.Damage += _enemyParams.damage;
            z.enabled = true;
        }
        _enemys[num].SetActive(true);
    }
}
