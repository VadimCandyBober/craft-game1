﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MageController : EnemyController
{

    private Enemy _mage;
    bool _playerFounded;
    float _distanceToPlayer;

    private GameObject _player;
    private Transform I;
    [Header("Teleport Properties")]
    public GameObject _teleportAnim;
    public Transform _enemy;
    public float _teleportDelay;
    public float reloadTime;
    public float _teleporRange;
    public float _teleportReactionDistance;
    private bool teleporReady = true;
    [Header("FireBall Properties")]
    public GameObject _fireBall;
    public Transform _fireBallPoint;
    public int _fireBallDamage;
    public float _fireBallSpeed;
    public float _fireBallDestroyTime;


    public override void SetUpClassProperties()
    {
        ArrayList mageParams = new ArrayList();
        mageParams.Add(_fireBall);
        mageParams.Add(_fireBallPoint);
        mageParams.Add(_fireBallDamage);
        mageParams.Add(_fireBallSpeed);
        mageParams.Add(_fireBallDestroyTime);
        mageParams.Add(_teleportAnim);
        mageParams.Add(_enemy);
        mageParams.Add(_teleportDelay);

        _mage = new Enemy(new Mage(mageParams));
    }

    public void SpellUse()
    {
        Vector3 pos = EnemyHelper._instance.GetRandomPoint();
         _mage.SpellUse(pos);
   
    }

    public void WeaponUse()
    {
        _mage.Hit();
    }

    // Use this for initialization
    void Start()
    {
        SetUpClassProperties();
        I = transform;
    }

    IEnumerator TeleportRearm()
    {
        yield return new WaitForSeconds(reloadTime);
        teleporReady = true;
    }


    IEnumerator playerDistanceUpdate()
    {
        _distanceToPlayer = Vector3.Distance(I.position, _player.transform.position);
        yield return new WaitForSeconds(0.2f);
        if (_distanceToPlayer < _teleportReactionDistance && teleporReady)
        {
            SpellUse();
            StartCoroutine(TeleportRearm());
            teleporReady = false;
        }
        StartCoroutine(playerDistanceUpdate());
    }

    IEnumerator FireBallAttack()
    {
        _distanceToPlayer = Vector3.Distance(I.position, _player.transform.position);
        transform.LookAt(_player.transform);
        if (_distanceToPlayer > _teleportReactionDistance )
        WeaponUse();
        yield return new WaitForSeconds(1f);
        StartCoroutine(FireBallAttack());
    }

	void Update () 
    {
        if(_player == null)
        {
            _player = GameObject.FindWithTag("Player");
        } else if(!_playerFounded)
        {
            StartCoroutine(playerDistanceUpdate());
            StartCoroutine(FireBallAttack());
            _playerFounded = true;
        }

	}
}
