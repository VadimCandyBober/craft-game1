﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorController : EnemyController {

    private Enemy _warrior;

    bool _playerFounded;
    float _distanceToPlayer;
    private ZombieSimpleAI _ai;


    [Header("Bow Props")]
    public float _dushDistance;
    public float _dushSpeed;
    public float _dushRearm;
    public float _dushUseTime;
    private bool _dushStarted;
    private bool _dushRearmed = true;

    private GameObject _player;
    private Transform I;
    private ZombieAnimatorControl _animator;

    public override void SetUpClassProperties()
    {
        ArrayList _warriorParams = new ArrayList();
        _warriorParams.Add(GetComponent<Pathfinding>());
        _warrior = new Enemy(new Warrior(_warriorParams));
    }
    private void Dush()
    {
        _warrior.SpellUse(_dushSpeed,_speed);
        _animator.Move();
    }

    private IEnumerator DushR()
    {
        _dushStarted = true;
        yield return new WaitForSeconds(_dushUseTime);
        _dushStarted = false;
    }

    private IEnumerator DushRearm()
    {
        yield return new WaitForSeconds(_dushRearm);
        _dushRearmed = true;
    }

    private void Awake()
    {
        I = transform;
        _animator = GetComponentInChildren<ZombieAnimatorControl>();
        SetUpClassProperties();
        _ai = GetComponent<ZombieSimpleAI>();
    }


    IEnumerator playerDistanceUpdate()
    {
        _distanceToPlayer = Vector3.Distance(I.position, _player.transform.position);
        yield return new WaitForSeconds(0.05f);
        transform.LookAt(_player.transform);
        if (_distanceToPlayer < _dushDistance && _dushRearmed)
        {
            StartCoroutine(DushRearm());
            StartCoroutine(DushR());
            _dushRearmed = false;
        }
        StartCoroutine(playerDistanceUpdate());
    }


    void Update()
    {
        if (_player == null)
        {
            _player = GameObject.FindWithTag("Player");
        }
        else if (!_playerFounded)
        {
            StartCoroutine(playerDistanceUpdate());
            _playerFounded = true;
        }

        if (_dushStarted) Dush();

    }
}
