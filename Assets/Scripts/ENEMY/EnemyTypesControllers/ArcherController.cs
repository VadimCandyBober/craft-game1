﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherController : EnemyController {
    
    private Enemy _archer;

    bool _playerFounded;
    float _distanceToPlayer;
    private ZombieSimpleAI _ai;

    [Header("Bow Properties")]
    public GameObject _arrow;
    public Transform _arrowPoint;
    public int _bowDamage;
    public float _arrowSpeed;
    public float _arrowDestroyTIme;
    public float fireDistance;

    [Header("Behavior model Properties")]
    public float _reactionDistance;
    public bool _behaviourStarted;

    private GameObject _player;
    private Transform I;
    private ZombieAnimatorControl _animator;

    public override void SetUpClassProperties()
    {
        ArrayList archerParams = new ArrayList();
        archerParams.Add(_arrow);
        archerParams.Add(_arrowPoint);
        archerParams.Add(_bowDamage);
        archerParams.Add(_arrowSpeed);
        archerParams.Add(_arrowDestroyTIme);
        archerParams.Add(I);

        _archer = new Enemy(new Archer(archerParams));
    }

    public void WeaponUse()
    {
        _animator.BowShoot();
        _archer.Hit();
    }
    public void BehaviorStart()
    {
        _archer.BehaviourStart();

    }

    public void BehaviorUpdate()
    {
        _archer.BehaviourUpdate();
        _animator.Move();
    }

    public bool GetBehaviourState()
    {
        return _behaviourStarted;
    }


    void Start()
    {
       
    }

    private void Awake()
    {
        I = transform;
        _animator = GetComponentInChildren<ZombieAnimatorControl>();
        SetUpClassProperties();
        _ai = GetComponent<ZombieSimpleAI>();
      

    }

    private void OnEnable()
    {
        if (_player)
        {
            _behaviourStarted = false;
            _ai.SetDefaaultSystem(_behaviourStarted);
            StartCoroutine(playerDistanceUpdate());
            StartCoroutine(ArrowAttack());
        }
    }

    IEnumerator playerDistanceUpdate()
    {
        _distanceToPlayer = Vector3.Distance(I.position, _player.transform.position);
        yield return new WaitForSeconds(0.05f);
        if (_distanceToPlayer < _reactionDistance && !_behaviourStarted)
        {
            BehaviorStart();
            _behaviourStarted = true;
            _ai.SetDefaaultSystem(_behaviourStarted);
         

        } else 
        {
            if (_distanceToPlayer > fireDistance || (_distanceToPlayer > _reactionDistance  + 5 && _behaviourStarted))
            {
                _behaviourStarted = false;
                _ai.SetDefaaultSystem(_behaviourStarted);
            }
        }
        StartCoroutine(playerDistanceUpdate());
    }

    IEnumerator ArrowAttack()
    {
        if (_distanceToPlayer < fireDistance && !_behaviourStarted)
        {
            transform.LookAt(_player.transform);
            _ai.SetDefaaultSystem(true);
            WeaponUse();
        }
        yield return new WaitForSeconds(1f);
        StartCoroutine(ArrowAttack());
    }

    void Update()
    {
        if (_player == null)
        {
            _player = GameObject.FindWithTag("Player");
        }
        else if (!_playerFounded)
        {
            StartCoroutine(playerDistanceUpdate());
            StartCoroutine(ArrowAttack());
            _playerFounded = true;
        }

        if (_behaviourStarted) BehaviorUpdate();
    }
}
