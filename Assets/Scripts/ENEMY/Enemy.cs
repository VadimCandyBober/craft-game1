﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy
{

    private Spell _spell;
    private Weapon _weapon;
    private Behavior _behaviour;
    public Enemy(EnemyFactory enemy)
    {
        _spell = enemy.CreateSpell();
        _weapon = enemy.CreateWeapon();
        _behaviour = enemy.CreateBehavior();
    }

    public void Hit()
    {
        _weapon.WeaponUse();
    }

    public void SpellUse(params object[] param)
    {
        _spell.SpellUse(param);
    }

    public void BehaviourStart()
    {
        _behaviour.BehaviorStart();
    }

    public void BehaviourUpdate()
    {
        _behaviour.BehaviorUpdate();
    }


}
