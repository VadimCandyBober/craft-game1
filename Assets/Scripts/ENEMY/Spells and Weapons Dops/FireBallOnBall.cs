﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallOnBall : MonoBehaviour {
    private float speed;
    private int damage;
    private float destroyTime;


    public void FireBallSetUp(params float[] param)
    {
        speed = param[0];
        damage = (int)param[1];
        destroyTime = param[2];
    }

    public void StartDestroy()
    {
        Destroy(gameObject, destroyTime);
    }

    private void FixedUpdate()
    {
        transform.position += transform.forward/10 * speed;
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            PlayerStats._instance.TakeDamage(damage);
        }
    }
}
