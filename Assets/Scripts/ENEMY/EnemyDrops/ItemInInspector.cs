﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemInInspector
{
    public int ID;
    public GameObject _Item;
    public int spawnChance;
    [HideInInspector]
    public float _RealySpawnChance;
}
