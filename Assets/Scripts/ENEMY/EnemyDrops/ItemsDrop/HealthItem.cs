﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthItem : DroppedItem {

    public int healthUp;

    public override void GetItem()
    {
        PlayerStats._instance.HpUp(healthUp);
        base.GetItem();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            GetItem();
    }
}
