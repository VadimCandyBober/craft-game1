﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppedItem : MonoBehaviour{
    public virtual void GetItem()
    {
        Destroy(gameObject);
    }

}
