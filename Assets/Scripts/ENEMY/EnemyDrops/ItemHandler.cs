﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHandler : MonoBehaviour {

    public List<ItemInInspector> _items = new List<ItemInInspector>();
    public int DropChance;
    private List<int> _itemsToRoll = new List<int>();
    public static ItemHandler _instance;
	void Start () {
        _instance = this;
        ChancesSetUp();
	}

    public void ChancesSetUp()
    {
        float sum = 0;
        for (int i = 0; i < _items.Count; i ++)
        {
            sum += _items[i].spawnChance;
        }
        for (int i = 0; i < _items.Count; i++)
        {
            _items[i]._RealySpawnChance = (DropChance / sum) * _items[i].spawnChance;
            Debug.Log(_items[i]._RealySpawnChance);
            for (int k = 0; k < _items[i]._RealySpawnChance; k++)
            {
                _itemsToRoll.Add(_items[i].ID);
            }
        }

    }


    public void ItemSpawn(Vector3 pos)
    {
        int ID = Random.Range(0,100);
        Debug.Log(ID + "+" + _itemsToRoll.Count);
        if (ID < _itemsToRoll.Count)
        {
            for (int i = 0; i < _items.Count; i++)
            {
                if (_itemsToRoll[ID] == _items[i].ID)
                {
                    Instantiate(_items[i]._Item, pos, _items[i]._Item.transform.rotation);
                    break;
                }
            }
        }
    }

	
	// Update is called once per frame
	void Update () {
		
	}
}
