﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warrior : EnemyFactory
{
    ArrayList _wParams;
    public Warrior(ArrayList _wParamsC)
    {
        _wParams = _wParamsC;
    }
    public override Behavior CreateBehavior()
    {
        return new WithoutModel();
    }

    public override Spell CreateSpell()
    {
        return new Dash((Pathfinding)_wParams[0]);
    }

    public override Weapon CreateWeapon()
    {
        return new Sword();
    }
}
