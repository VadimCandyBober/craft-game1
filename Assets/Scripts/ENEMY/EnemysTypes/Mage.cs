﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mage : EnemyFactory
{
    private ArrayList paramas = new ArrayList();

    public Mage(ArrayList classParams)
    {
        paramas = classParams;
    }

    public override Behavior CreateBehavior()
    {
        return new WithoutModel();
    }

    public override Spell CreateSpell()
    {
        return new Teleport((GameObject)paramas[5], (Transform)paramas[6],(float)paramas[7]);
    }

    public override Weapon CreateWeapon()
    {
        return new FireBall((GameObject)paramas[0], (Transform)paramas[1], (int)paramas[2], (float)paramas[3], (float)paramas[4]);
    }
}
