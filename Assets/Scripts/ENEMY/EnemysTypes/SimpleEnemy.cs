﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemy : EnemyFactory
{
    public override Behavior CreateBehavior()
    {
        return new WithoutModel();
    }

    public override Spell CreateSpell()
    {
        return new NotSpell();
    }

    public override Weapon CreateWeapon()
    {
        return new Arm();
    }
}
