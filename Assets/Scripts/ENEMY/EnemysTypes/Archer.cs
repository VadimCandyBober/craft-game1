﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Archer : EnemyFactory
{

    private ArrayList paramas = new ArrayList();

    public Archer(ArrayList classParams)
    {
        paramas = classParams;
    }

    public override Behavior CreateBehavior()
    {
        return new RunOut((Transform)paramas[5]);
    }

    public override Spell CreateSpell()
    {
        return new NotSpell();
    }

    public override Weapon CreateWeapon()
    {
        return new Bow((GameObject)paramas[0],(Transform)paramas[1],(int)paramas[2],(float)paramas[3],(float)paramas[4]);
    }
}
