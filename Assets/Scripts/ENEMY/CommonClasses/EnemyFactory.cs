﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyFactory 
{
    public abstract Spell CreateSpell();
    public abstract Weapon CreateWeapon();
    public abstract Behavior CreateBehavior();
}
