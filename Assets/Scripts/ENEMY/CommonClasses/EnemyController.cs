﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyController : MonoBehaviour{

    public float _health;
    public float _speed;
    public float _mageResist;
    public float _armor;



    public abstract void SetUpClassProperties();
 
}
