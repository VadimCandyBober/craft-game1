﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Behavior : MonoBehaviour{
    public abstract void BehaviorStart();
    public abstract void BehaviorUpdate();
}
