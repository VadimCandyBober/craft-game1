﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyHelper : MonoBehaviour {


    public Transform[] pointsToRunOut;
    public Transform[] _archer;

    public static EnemyHelper _instance;

    public void Start()
    {
        _instance = this;
    }

    public Vector3 GetRandomPoint()
    {
        return pointsToRunOut[Random.Range(0,pointsToRunOut.Length)].position;
    }

    public Vector3 GetRandomPointArcher()
    {
        Vector3 _playerPos = PlayerStats._instance.gameObject.transform.position;
        Vector3 pos = _archer[0].position;
        for (int i = 0; i < _archer.Length; i ++)
        {
            if (Vector3.Distance(pos, _playerPos) > Vector3.Distance(pos, _archer[i].position)) pos = _archer[i].position;
        }
        return pos;
    }



}
