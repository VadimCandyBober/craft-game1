﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : Weapon
{
    private GameObject _fireBall ;
    private Transform _fireBallPoint;
    private int _fireBallDamage;
    private float _fireBallSpeed;
    private float _fireBallDestroyTime;

    public FireBall(GameObject FIREBALL, Transform POINT, int DAMAGE, float SPEED, float DESTROY)
    {
        _fireBall = FIREBALL;
        _fireBallPoint = POINT;
        _fireBallDamage = DAMAGE;
        _fireBallSpeed = SPEED;
        _fireBallDestroyTime = DESTROY;

    }
    public override void WeaponUse()
    {

        GameObject fireball = Instantiate(_fireBall, _fireBallPoint.position, _fireBallPoint.rotation);
        fireball.GetComponent<FireBallOnBall>().FireBallSetUp(_fireBallSpeed,_fireBallDamage,_fireBallDestroyTime);
        fireball.GetComponent<FireBallOnBall>().StartDestroy();
    }
}


