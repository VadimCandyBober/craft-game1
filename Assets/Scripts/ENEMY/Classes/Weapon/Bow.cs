﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : Weapon
{
    private GameObject _arrow;
    private Transform _arrowPoint;
    private int _arrowDamage;
    private float _arrowSpeed;
    private float _arrowDestoryTime;


    public Bow(GameObject ARROW, Transform POINT, int DAMAGE, float SPEED, float DESTROY)
    {
        _arrow = ARROW;
        _arrowPoint = POINT;
        _arrowDamage = DAMAGE;
        _arrowSpeed = SPEED;
        _arrowDestoryTime = DESTROY;

    }
    public override void WeaponUse()
    {

        GameObject fireball = Instantiate(_arrow, _arrowPoint.position, _arrowPoint.rotation);
        fireball.GetComponent<ArrowInGame>().ArrowSetUp(_arrowSpeed, _arrowDamage, _arrowDestoryTime);
        fireball.GetComponent<ArrowInGame>().StartDestroy();
    }

}
