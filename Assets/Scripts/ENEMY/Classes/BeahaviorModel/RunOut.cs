﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class RunOut : Behavior
{
    private Transform I;
    private Vector3 _pointToRun;
    Pathfinding _agent;

    public RunOut(Transform _enemy)
    {
        I = _enemy;
    }
    
    public override void BehaviorStart()
    {
        _pointToRun = EnemyHelper._instance.GetRandomPointArcher();
        _agent = I.GetComponent<Pathfinding>();
        _agent.NavigateToRandomPoint(_pointToRun);
    }

    public override void BehaviorUpdate()
    {
        _agent.NavigateToRandomPoint(_pointToRun);
        Debug.Log("Run " + _pointToRun);

     
    }
}
