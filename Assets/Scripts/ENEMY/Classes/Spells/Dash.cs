﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class Dash : Spell {

    public Pathfinding p;

    public Dash(Pathfinding _p)
    {
        p = _p;
    }
    public override void SpellUse(params object[] param)
    {
        p.Speed = (float)param[0];
        if (Vector3.Distance(p.transform.position, PlayerStats._instance.transform.position) < 4)
            p.Speed = (float)param[1];

    }
}
