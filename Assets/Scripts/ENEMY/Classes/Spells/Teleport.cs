﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;

class Teleport : Spell 
{
    private GameObject _teleportAnim;
    private Transform _enemy;
    private float _teleportDelay;

    private Vector3 pos;

    public Teleport(GameObject ANIM, Transform ENEMY, float DELAY)
    {
        _teleportAnim = ANIM;
        _enemy = ENEMY;
        _teleportDelay = DELAY;
    }


    public override void SpellUse(params object[] param)
    {
        pos = (Vector3)param[0];
        TeleportStart();
    }

    public async Task TeleportStart()
    {
        Instantiate(_teleportAnim, _enemy.position, Quaternion.identity);
        Instantiate(_teleportAnim, pos, Quaternion.identity);
        await Task.Delay((int)_teleportDelay);

        _enemy.transform.position = pos;
    }

}
