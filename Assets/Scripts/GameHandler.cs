﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameHandler : MonoBehaviour {
    public static GameHandler _instance;

    public GameObject _Player;
    public Transform _spawn;

    public GameObject WorldLoadingUI;
    public GameObject PlayerUI;

	void Start () {
        _instance = this;	
	}

    public void OnWorldGenerated()
    {
        StartCoroutine(PlayerCreate());
        EnemyManager._instance.StartSpawnEnemy();

    }

    public IEnumerator PlayerCreate()
    {
        WorldLoadingUI.SetActive(false);
        PlayerUI.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        Instantiate(_Player, _spawn.position, Quaternion.identity);
        Debug.Log("PlayerSpawned");
    }

}
