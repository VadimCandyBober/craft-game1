﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour {
    
    private Animator _anim;
    public static PlayerAnimator _instance;
    // Use this for initialization
    void Start()
    {
        _instance = this;
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Attack()
    {
        _anim.SetInteger("State", 2);
    }
    public void Move()
    {
        _anim.SetInteger("State", 1);
    }

    public void Idle()
    {
        _anim.SetInteger("State", 0);
    }
}
