﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeManager : MonoBehaviour {

    public List<Mode> _GameModes;

    private int _changedMode = 0;

    public static ModeManager _instance;

    private void Awake()
    {
        _instance = this;

    }



    // Use this for initialization
    void Start () {
      
	}
    public int GetChangedMode()
    {
        return _changedMode;
    }
    public void ModeSetUp()
    {
        _changedMode = PlayerPrefs.GetInt("Mode");
        _GameModes[_changedMode].StartMode();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
