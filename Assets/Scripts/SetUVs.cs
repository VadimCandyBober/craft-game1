﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetUVs : MonoBehaviour {
    public float pixelSize = 16;
    public float tileX = 1;
    public float tileY = 1;
	// Use this for initialization
    public void StetUP () {
        float tilePerc = 1 / pixelSize;
        float umin = tilePerc * tileX;
        float umax = tilePerc * (tileX + 1);
        float vmin = tilePerc * tileY;
        float vmax = tilePerc * (tileY + 1);

        Vector2[] bloackUVs = new Vector2[24];
        bloackUVs[0] = new Vector2(umin, vmin);
        bloackUVs[1] = new Vector2(umax, vmin); 
        bloackUVs[2] = new Vector2(umin, vmax); 
        bloackUVs[3] = new Vector2(umax, vmax); 
        bloackUVs[4] = new Vector2(umin, vmax); 
        bloackUVs[5] = new Vector2(umax, vmax); 
        bloackUVs[6] = new Vector2(umin, vmax); 
        bloackUVs[7] = new Vector2(umax, vmax); 
        bloackUVs[8] = new Vector2(umin, vmin); 
        bloackUVs[9] = new Vector2(umax, vmin);
        bloackUVs[10] = new Vector2(umin, vmin); 
        bloackUVs[11] = new Vector2(umax, vmin); 
        bloackUVs[12] = new Vector2(umin, vmin); 
        bloackUVs[13] = new Vector2(umin, vmax); 
        bloackUVs[14] = new Vector2(umax, vmax); 
        bloackUVs[15] = new Vector2(umin, vmin); 
        bloackUVs[16] = new Vector2(umin, vmin); 
        bloackUVs[17] = new Vector2(umin, vmax); 
        bloackUVs[18] = new Vector2(umax, vmax);
        bloackUVs[19] = new Vector2(umax, vmin); 
        bloackUVs[20] = new Vector2(umin, vmin); 
        bloackUVs[21] = new Vector2(umin, vmax); 
        bloackUVs[22] = new Vector2(umax, vmax); 
        bloackUVs[23] = new Vector2(umax, vmin); 

        this.GetComponent<MeshFilter>().mesh.uv = bloackUVs;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
