﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Pathfinding : MonoBehaviour {
    [HideInInspector]
    public Transform seeker;
    public GameObject target;
    [HideInInspector]
    public Grid grid;
    [HideInInspector]
    public List<Node> path = new List<Node>();
    [Header("Скорость юнита")]
    public float Speed;
    [Header("Скорость юнита")]
    public float RotationSpeed;

    void Awake()
    {
        seeker = gameObject.transform;
        startg();
    }

    public void startg()
    {
        grid = GameObject.FindGameObjectWithTag("S").GetComponent<Grid>();
    }

    public void NavigateToPlayer()
    {
        if (grid != null && target)
            FindPath(seeker.position, target.transform.position);
    }

    public void NavigateToRandomPoint(Vector3 pos)
    {
        if (grid != null && target)
            FindPath(seeker.position, pos);
    }

    void Update()
    {
        if(!target) target = GameObject.FindGameObjectWithTag("Player");
     
    }

    public void FindPath(Vector3 startPos, Vector3 targetPos)
    {
        Node startNode = grid.NodeFromWorldPoint(startPos);
        Node targetNode = grid.NodeFromWorldPoint(targetPos);

        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();
        openSet.Add(startNode);

        while (openSet.Count > 0)
        {
            Node node = openSet[0];
            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < node.fCost || openSet[i].fCost == node.fCost)
                {
                    if (openSet[i].hCost < node.hCost)
                        node = openSet[i];
                }
            }

            openSet.Remove(node);
            closedSet.Add(node);

            if (node == targetNode)
            {
                RetracePath(startNode, targetNode);
                return;
            }

            foreach (Node neighbour in grid.GetNeighbours(node))
            {
                if (!neighbour.walkable || closedSet.Contains(neighbour))
                {
                    continue;
                }

                int newCostToNeighbour = node.gCost + GetDistance(node, neighbour);
                if (newCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                {
                    neighbour.gCost = newCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, targetNode);
                    neighbour.parent = node;

                    if (!openSet.Contains(neighbour))
                        openSet.Add(neighbour);
                }
            }
        }
    }

    void RetracePath(Node startNode, Node endNode)
    {
        path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        path.Reverse();

        grid.path = path;
        if (path.Count > 0)
        {
            float step = Speed * Time.deltaTime;
            seeker.transform.position = Vector3.MoveTowards(seeker.transform.position, path[0].worldPosition, step);
            Quaternion q = Quaternion.LookRotation(path[0].worldPosition - seeker.transform.position);
            seeker.transform.rotation = Quaternion.Slerp(seeker.transform.rotation, q, Time.deltaTime * RotationSpeed);

        }
    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        return 14 * dstX + 10 * (dstY - dstX);
    }
}
 


