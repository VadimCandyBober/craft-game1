﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerUiHandler : MonoBehaviour {

    public Image healthBar;
    public Text _pointText;
    public static PlayerUiHandler _instance;
	// Use this for initialization
	void Start () {
        _instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void HealthBarUpdate(float value)
    {
        healthBar.fillAmount = value;
    }

    public void PointsUpdate(int value)
    {
        _pointText.text = value.ToString();
    }
}
