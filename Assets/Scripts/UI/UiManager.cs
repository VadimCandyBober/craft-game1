﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiManager : MonoBehaviour {

    public static UiManager _instance;
    [SerializeField]
    GameObject _PlayerDieUI;
	void Start () {
        _instance = this;
	}

	void Update () {
		
	}

    public void PlayerDieUI()
    {
        _PlayerDieUI.SetActive(!_PlayerDieUI.active);
    }
}
