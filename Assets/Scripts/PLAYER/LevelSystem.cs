﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelSystem : MonoBehaviour {



    private int nowLevel;
    private int toNextLevel;

    public Image _levelBar;

    public List<Level> _levels = new List<Level>();

    private float _levelPercent ;

    public int _nowProgress;



    private void Awake()
    {
        nowLevel = PlayerPrefs.GetInt("Level",1);
        _nowProgress = PlayerPrefs.GetInt("Progress", 0);
    }

    private void PercentSetUp()
    {
        toNextLevel = _levels[nowLevel].ToNextLevel;
        _levelPercent = 1.0f /toNextLevel ;
    }

    public void ExpAdd(int exp)
    {
        _nowProgress += exp;
        if (_nowProgress > toNextLevel)
        {
            nowLevel++;
            _nowProgress -= toNextLevel;
            PercentSetUp();
            PlayerPrefs.SetInt("Level",nowLevel);
            PlayerPrefs.Save();
        }
        _levelBar.fillAmount = _levelPercent * _nowProgress;
    }

    // Update is called once per frame
    void Update () 
    {
        
	}
}
