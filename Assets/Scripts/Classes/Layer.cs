﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LayerForward {
    public bool _isHole;
    public Vector2 uvs;
}

[System.Serializable]
public class LayersForwad
{
    public List<LayerForward> layers = new List<LayerForward>();
}
[System.Serializable]
public class LayersUps
{
    public List<LayersForwad> _layersUp = new List<LayersForwad>();
}

