﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthItems : Item {


    public int SpawnChance;
    public int healthUp;
    public GameObject ItemObj;
    public int ItemId;

	// Use this for initialization
	void Start () {
        spawnChance = SpawnChance;
        itemObj = ItemObj;
        _ID = ItemId;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            GetItem();
    }

    public override void GetItem()
    {
        PlayerStats._instance.HpUp(healthUp);
        base.GetItem();
    }
}
