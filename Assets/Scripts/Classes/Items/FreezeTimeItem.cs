﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeTimeItem : Item {

    public int SpawnChance;
    public GameObject ItemObj;
    public int ItemId;
    public float TimeFreeze;
    // Use this for initialization
    void Start()
    {
        spawnChance = SpawnChance;
        itemObj = ItemObj;
        _ID = ItemId;
    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            GetItem();
    }

    public override void GetItem()
    {
        StartCoroutine(timeFreeze());
        base.GetItem();
    }

    IEnumerator timeFreeze()
    {
        List<GameObject> _enemys = EnemyManager._instance.GetAllEnemys();
        for (int i = 0; i < _enemys.Count; i++)
        {
            _enemys[i].GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
            _enemys[i].GetComponent<ZombieSimpleAI>().enabled = false;
        }
        yield return new WaitForSeconds(TimeFreeze);
        for (int i = 0; i < _enemys.Count; i++)
        {
            _enemys[i].GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
            _enemys[i].GetComponent<ZombieSimpleAI>().enabled = true;
        }
    }
}
