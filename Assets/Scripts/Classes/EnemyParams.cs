﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyParams{

    public int hp;
    public int damage;
	
}
