﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item : MonoBehaviour {

    public int spawnChance;
    public GameObject itemObj;
    public int _ID;

    public virtual void GetItem()
    {
        Destroy(gameObject); 
    }

	
}
